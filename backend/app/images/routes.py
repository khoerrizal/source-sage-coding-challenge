from flask import request
import json
import base64
from config import Config
from app.images import bp
from app.extensions import db
from app.models import Image
from uuid import uuid4
from datetime import datetime
image_api = bp

@image_api.route('/', methods=['POST'])
def entities():
    if request.method == "POST":
        img = Image.upload_image(request.json['data'], request.json.get('product_id'), request.json.get('variant_id'))

        return {
            'message': 'Success create image',
            'data': {
                'id': img.id
            }
        }

@image_api.route('/<int:id>', methods=['DELETE'])
def entity(id):
    if request.method == "DELETE":
        obj = Image.query.get(id)
        db.session.delete(obj)
        db.session.commit()
        return {
            'message': 'Success',
        }
