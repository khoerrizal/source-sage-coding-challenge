from datetime import datetime
from app.extensions import db
from config import Config
import base64
from uuid import uuid4

class Image(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(225))
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    variant_id = db.Column(db.Integer, db.ForeignKey('variant.id'))

    def __str__(self) -> str:
        return self.url

    def upload_image(image_str, product_id=None, variant_id=None):
        imgdata = base64.b64decode(image_str)
        filename = f'{Config.UPLOAD_FOLDER[1:]}{uuid4().hex}.jpg'
        with open(filename, 'wb') as f:
            f.write(imgdata)
        filename = filename.replace("./", "/api/")
        image = Image(
            url=filename,
            product_id=product_id if product_id and product_id != "" else None,
            variant_id=variant_id if variant_id and variant_id != "" else None
        )
        db.session.add(image)
        db.session.commit()
        return image

class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    description = db.Column(db.Text)
    logo_id = db.Column(db.Integer, db.ForeignKey('image.id'))
    images = db.relationship('Image', backref="product", lazy=True, foreign_keys=Image.product_id)
    created_at = db.Column(db.DateTime, nullable=False,
        default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, nullable=False,
        default=datetime.utcnow)
    
    def __str__(self) -> str:
        return self.name

    def getdata(self):
        logo_url = ""
        if self.logo_id:
            logo_obj = Image.query.get(self.logo_id)
            logo_url = f'{Config.HOST}{logo_obj.url[1:]}'
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description,
            'logo': logo_url,
            'images': [{'url': f'{Config.HOST}{x.url[1:]}', 'id': x.id} for x in self.images],
            'created_at': self.created_at,
            'updated_at': self.updated_at
        }

class Variant(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    name = db.Column(db.String(100))
    size = db.Column(db.String(100))
    color = db.Column(db.String(100))
    images = db.relationship('Image', backref="variant", lazy=True, foreign_keys=Image.variant_id)
    created_at = db.Column(db.DateTime, nullable=False,
        default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, nullable=False,
        default=datetime.utcnow)

    def __str__(self) -> str:
        return self.name

    def getdata(self):
        return {
            'id': self.id,
            'product_id': self.product_id,
            'name': self.name,
            'color': self.color,
            'size': self.size,
            'images': [{'url': f'{Config.HOST}{x.url[1:]}', 'id': x.id} for x in self.images],
            'created_at': self.created_at,
            'updated_at': self.updated_at
        }