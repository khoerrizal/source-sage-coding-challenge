from flask import Blueprint

bp = Blueprint('variants', __name__)

from app.variants import routes