from flask import request

from app.variants import bp
from app.models import Variant, Image, Product
from app.extensions import db

variant_api = bp

@variant_api.route('/', methods=['GET', 'POST'])
def entities():
    if request.method == "GET":
        data = Variant.query.all()
        return {
            'data': [x.getdata() for x in data]
        }
    elif request.method == "POST":
        product_related = Product.query.get(request.json['product_id'])
        new_variant = Variant(
            name = f'{product_related.name} ({request.json["color"]}, {request.json["size"]})',
            product_id = request.json['product_id'],
            size = request.json['size'],
            color = request.json['color'],
        )
        db.session.add(new_variant)
        db.session.commit()

        if "images" in request.json:
            for img in request.json['images']:
                Image.upload_image(img, variant_id = new_variant.id)
        return {
            'message': 'Success create variant',
            'data': {
                'id': new_variant.id
            }
        }

@variant_api.route('/<int:id>', methods=['GET', 'PUT', 'DELETE'])
def entity(id):
    if request.method == "GET":
        obj = Variant.query.get(id)
        return obj.getdata()
    elif request.method == "PUT":
        obj = Variant.query.get(id)
        product_related = Product.query.get(obj.product_id)
        obj.size = request.json['size']
        obj.color = request.json['color']
        obj.name = f'{product_related.name} ({request.json["color"]}, {request.json["size"]})'
        db.session.commit()
        return {
            'id': id,
            'message': 'Success',
        }
    elif request.method == "DELETE":
        obj = Variant.query.get(id)
        db.session.delete(obj)
        db.session.commit()
        return {
            'message': 'Success',
        }