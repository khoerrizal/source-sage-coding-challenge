from flask import send_from_directory
from app.main import bp
import app
from config import Config


@bp.route('/')
def index():
    return 'This is API from Khoerurrizal'

@bp.route('/api/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(Config.UPLOAD_FOLDER,
                               filename)