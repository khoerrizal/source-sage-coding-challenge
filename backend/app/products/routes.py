from flask import request
import json
import base64
from config import Config
from app.products import bp
from app.extensions import db
from app.models import Product, Image
from uuid import uuid4
from datetime import datetime
product_api = bp

@product_api.route('/', methods=['GET', 'POST'])
def entities():
    if request.method == "GET":
        data = Product.query.all()
        return {
            'data': [x.getdata() for x in data]
        }
    elif request.method == "POST":
        logo = None
        if request.json.get("logo"):
            logo = Image.upload_image(request.json['logo'])
        new_product = Product(
            name = request.json["name"],
            description = request.json["description"],
            logo_id = logo.id if logo else None
        )
        db.session.add(new_product)
        db.session.commit()

        if "images" in request.json:
            for img in request.json['images']:
                Image.upload_image(img, new_product.id)
        return {
            'message': 'Success create product',
            'data': {
                'id': new_product.id
            }
        }

@product_api.route('/<int:id>', methods=['GET', 'PUT', 'DELETE'])
def entity(id):
    if request.method == "GET":
        return Product.query.get(id).getdata()
    elif request.method == "PUT":
        obj = Product.query.get(id)
        obj.name = request.json['name']
        obj.description = request.json['description']
        obj.updated_at = datetime.now()
        if request.json.get("logo"):
            logo = Image.upload_image(request.json['logo'])
            obj.logo_id = logo.id
        db.session.commit()
        return {
            'id': id,
            'message': 'Success',
        }
    elif request.method == "DELETE":
        obj = Product.query.get(id)
        db.session.delete(obj)
        db.session.commit()
        return {
            'message': 'Success',
        }
