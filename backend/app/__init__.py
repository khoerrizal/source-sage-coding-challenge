from flask import Flask
from app.extensions import db
from config import Config

def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    # Initialize Flask extensions here
    db.init_app(app)

    # Register blueprints here
    from app.main import bp as main_bp
    app.register_blueprint(main_bp)

    from app.products import bp as products_bp
    app.register_blueprint(products_bp, url_prefix='/api/products')

    from app.variants import bp as variants_bp
    app.register_blueprint(variants_bp, url_prefix='/api/variants')

    from app.images import bp as images_bp
    app.register_blueprint(images_bp, url_prefix='/api/images')

    @app.route('/test/')
    def test_page():
        return '<h1>Testing the Flask Application Factory Pattern</h1>'
    
    with app.app_context():
        db.create_all()

    return app