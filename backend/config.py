class Config:
    SECRET_KEY = "dd80db01-974a-4dfb-9ca7-69699f0a2452"
    SQLALCHEMY_DATABASE_URI = 'mysql://root:rizal@db:3306/rizaldb'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    UPLOAD_FOLDER = "../uploads/"
    HOST = "http://localhost:5000"