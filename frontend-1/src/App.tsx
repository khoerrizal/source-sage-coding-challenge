import React, { useState } from 'react';
import {
  InboxOutlined,
  CodeSandboxOutlined,
} from '@ant-design/icons';
import type { MenuProps } from 'antd';
import { Breadcrumb, Layout, Menu, theme } from 'antd';
import ProductList from './components/ProductList';

const { Header, Content, Footer, Sider } = Layout;

type MenuItem = Required<MenuProps>['items'][number];

function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[],
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
  } as MenuItem;
}

const items: MenuItem[] = [
  getItem('Product', 'productMenu', <InboxOutlined />, [
    getItem('Create', 'product-create'),
    getItem('View', 'product-view'),
  ]),
  getItem('Variant', 'variantMenu', <CodeSandboxOutlined />, [
    getItem('Create', 'variant-create'),
    getItem('View', 'variant-view'),
  ]),
];

const App: React.FC = () => {
    const [collapsed, setCollapsed] = useState(false);
    const [selectedMenu, setSelectedMenu] = useState<string>("")
    const {
        token: { colorBgContainer },
    } = theme.useToken();

    return (
        <Layout style={{ minHeight: '100vh' }}>
        <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
            <div style={{ height: 32, margin: 16, background: 'rgba(255, 255, 255, 0.2)' }} />
            <Menu onClick={(event) => setSelectedMenu(event.key)} theme="dark" defaultSelectedKeys={['1']} mode="inline" items={items} />
        </Sider>
        <Layout className="site-layout">
            <Header style={{ padding: 0, background: colorBgContainer }} />
            <Content style={{ margin: '0 16px' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                    {selectedMenu.split("-").map((submenustring) => (
                        <Breadcrumb.Item>{submenustring.toLocaleUpperCase()}</Breadcrumb.Item>
                    ))}
                </Breadcrumb>
                <div style={{ padding: 24, minHeight: 360, background: colorBgContainer }}>
                    {selectedMenu === "product-view" && (
                        <ProductList/>
                    )}
                </div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>copyright KHOERURRIZAL@2022</Footer>
        </Layout>
        </Layout>
    );
};

export default App;