import { IApiRequest } from './types'
import axios from 'axios'
import { createHeader, getContentType } from '../utils/fetchApi'

export const apiInstance = axios.create({
    baseURL: process.env.MAIN_BASE_URL,
})

class ApiRequest {
    static request = async (method = 'GET', payload: IApiRequest = {}) => {
        const contentType = getContentType(payload.type)
        const baseHeaders = { 'Content-Type': contentType }
        const headers = createHeader(payload.headers, baseHeaders)
        const url = payload.url
        const data = payload.body ? payload.body : {}
        const requestObj = { url, headers, method, data }

        try {
            const response = await apiInstance.request(requestObj)
            return response
        } catch (err) {
            console.log(err, "error on request api")
            throw err
        }
    }

    static get = (payload: IApiRequest) => this.request('GET', payload)

    static put = (payload: IApiRequest) => this.request('PUT', payload)

    static post = (payload: IApiRequest) => this.request('POST', payload)

    static delete = (payload: IApiRequest) => this.request('DELETE', payload)

    static patch = (payload: IApiRequest) => this.request('PATCH', payload)
}

export default ApiRequest
