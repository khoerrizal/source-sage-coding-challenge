export interface IApiRequest {
    path?: string
    params?: string
    url?: string
    type?: 'form-data' | 'json'
    headers?: {
        [key: string]: any
    }
    body?: Object
}
