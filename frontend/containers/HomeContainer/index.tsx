import { useEffect } from "react"
import ProductForm from "../../components/atoms/ProductForm"
import VariantForm from "../../components/atoms/VariantForm"
import ProductList from "../../components/molecules/ProductList"
import VariantList from "../../components/molecules/VariantList"
import { useGlobalContext } from "../../providers/globalProvider"

const HomeContainer: React.FC = () => {
    const {
        contentState: {
            selectedMenu
        }
    } = useGlobalContext()

    useEffect(() => {
        console.log(selectedMenu, "selectedMenu <<<<<<<")
    }, [selectedMenu])
    return (
        <div className="tesssssss">
            {selectedMenu.startsWith("product-view") && (
                <ProductList/>
            )}
            {selectedMenu.startsWith("variant-view") && (
                <VariantList/>
            )}
            {selectedMenu.startsWith("product-create") && (
                <ProductForm/>
            )}
            {selectedMenu.startsWith("variant-create") && (
                <VariantForm/>
            )}
        </div>
    )
}

export default HomeContainer