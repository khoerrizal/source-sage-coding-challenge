import { Inter } from '@next/font/google'
import AppLayout from '../../components/templates/AppLayout'
import VariantContainer from '../../containers/VariantContainer'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
    return (
        <AppLayout title={'Detail Variant'} description={'Page detail variant'}>
            <VariantContainer/>
        </AppLayout>
    )
}
