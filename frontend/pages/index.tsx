import { Inter } from '@next/font/google'
import AppLayout from '../components/templates/AppLayout'
import HomeContainer from '../containers/HomeContainer'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
    return (
        <AppLayout title={'Product Management'} description={'CRUD Product and Variant'}>
            <HomeContainer/>
        </AppLayout>
    )
}
