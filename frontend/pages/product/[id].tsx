import { Inter } from '@next/font/google'
import AppLayout from '../../components/templates/AppLayout'
import ProductContainer from '../../containers/ProductContainer'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
    return (
        <AppLayout title={'Detail Product'} description={'Page detail product'}>
            <ProductContainer/>
        </AppLayout>
    )
}
