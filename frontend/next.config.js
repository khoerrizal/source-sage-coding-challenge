/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    env: {
        MAIN_BASE_URL: process.env.MAIN_BASE_URL,
    },
};

module.exports = nextConfig;
