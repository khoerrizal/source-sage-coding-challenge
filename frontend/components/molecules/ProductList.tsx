import React, { useEffect, useState } from 'react';
import { Space, Table, Tag } from 'antd';
import type { ColumnsType } from 'antd/lib/table';
import { useGlobalContext } from '../../providers/globalProvider';
import Link from 'next/link';

interface DataType {
    id: string,
    key: string;
    name: string;
    description: string;
}

const columns: ColumnsType<DataType> = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        render: (_, record) => (
            <Space size="middle">
                <Link href={`/product/${record.id}`}>{record.name}</Link>
            </Space>
        )
    },
    {
        title: 'Description',
        dataIndex: 'description',
        key: 'description',
    },
];

const ProductList: React.FC = () => {
    const [data, setData] = useState<DataType[]>([])
    const {
        contentState: {
            productList,
            getProductList
        }
    } = useGlobalContext()
    
    useEffect(() => {
        if (productList.length > 0){
            setData(productList.map((line) => {
                return {
                    id: line.id.toString(),
                    key: line.id.toString(),
                    name: line.name,
                    description: line.description,
                }
            }))
        }
    }, [productList])

    useEffect(() => {
        getProductList()
    }, [])
    return (
        <Table columns={columns} dataSource={data} />
    )
};

export default ProductList;