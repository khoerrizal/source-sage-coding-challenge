import React, { useEffect, useState } from 'react';
import { Space, Table, Tag } from 'antd';
import type { ColumnsType } from 'antd/lib/table';
import { useGlobalContext } from '../../providers/globalProvider';
import Link from 'next/link';

interface DataType {
    id: string,
    key: string;
    productId: string;
    name: string;
    color: string;
    size: string;
}

const columns: ColumnsType<DataType> = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        render: (_, record) => (
            <Space size="middle">
                <Link href={`/variant/${record.id}`}>{record.name}</Link>
            </Space>
        )
    },
    {
        title: 'Color',
        dataIndex: 'color',
        key: 'color',
    },
    {
        title: 'Size',
        dataIndex: 'size',
        key: 'size',
    },
];

interface variantListProps {
    productId?: string
}

const VariantList: React.FC<variantListProps> = ({ productId="" }) => {
    const [data, setData] = useState<DataType[]>([])
    const {
        contentState: {
            variantList,
            getVariantList
        }
    } = useGlobalContext()
    useEffect(() => {
        if (variantList.length > 0){
            if (productId){
                setData(variantList.map((line) => {
                    return {
                        id: line.id.toString(),
                        key: line.id.toString(),
                        productId: line.product_id.toString(),
                        name: line.name,
                        color: line.color,
                        size: line.size
                    }
                }).filter((line)=>{
                    return line.productId == productId
                }))
            }else{
                setData(variantList.map((line) => {
                    return {
                        id: line.id.toString(),
                        key: line.id.toString(),
                        productId: line.product_id.toString(),
                        name: line.name,
                        color: line.color,
                        size: line.size
                    }
                }))
            }
        }
    }, [variantList])

    useEffect(() => {
        getVariantList()
    }, [])
    return (
        <Table columns={columns} dataSource={data} />
    )
};

export default VariantList;