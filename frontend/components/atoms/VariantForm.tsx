import { Button, Form, Input, Select, Space, Tooltip } from 'antd';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { useGlobalContext } from '../../providers/globalProvider';
import UploadImageList from './UploadImageList';
import { DeleteOutlined } from '@ant-design/icons';
import { Image } from 'antd';

const { Option } = Select;

interface optionProps {
    value: string, 
    label: string
}

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },   
};
const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
};

const VariantForm: React.FC = () => {
    const {
        contentState: {
            setSelectedMenu,
            selectedMenu,
            variantDetail,
            getVariantDetail,
            productList,
            createVariant,
            updateVariant,
            getVariantList,
            deleteVariant,
            imagesString,
            createImage,
            deleteImage,
            setImagesString,
            setFileList,
            setVariantDetail
        }
    } = useGlobalContext()
    const [form] = Form.useForm();
    const [optionProduct, setOptionProduct] = useState<optionProps[]>([])

    const onFinish = async (values: any) => {
        console.log(variantId, "<<<<<<<< cek variantId")
        if (variantId){
            await updateVariant(variantId, values.productId, values.color, values.size)
            imagesString.map(async (img) => {
                await createImage(img, "", variantId)
            })
            setImagesString([])
            setFileList([])
            getVariantList()
            await setSelectedMenu("variant-view")
            router.push("/")
            return
        }{
            await createVariant(values.productId, values.color, values.size, imagesString)
            getVariantList()
            setImagesString([])
            setFileList([])
            await setSelectedMenu("variant-view")
            router.push("/")
            return
        }
        
    };

    const onDelete = async () => {
        deleteVariant(variantId)
        getVariantList()
        await setSelectedMenu("variant-view")
        router.push("/")
    };

    const [variantId, setVariantId] = useState<string | string[]>("")

    const router = useRouter()

    useEffect(() => {
        if (router.isReady && router.query.id){
            setVariantId(router.query.id)
            getVariantDetail(router.query.id)
        }else{
            setVariantDetail(null)
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [router.isReady, router.query.id])

    useEffect(() => {
        console.log(variantDetail, "<<< cek variant detail")
        form.setFieldsValue({ size: variantDetail?.size });
        form.setFieldsValue({ color: variantDetail?.color });
        form.setFieldsValue({ productId: variantDetail?.product_id.toString() });
    }, [variantDetail])

    useEffect(() => {
        if (selectedMenu != "variant-create"){
            if (variantId){
                setSelectedMenu(`variant-view-${variantId}`)
            }else{
                setSelectedMenu(`variant-view`)
            }
        }
    }, [variantId, selectedMenu])

    useEffect(() => {
        if (productList.length > 0){
            setOptionProduct(productList.map((line) => {
                return {
                    value: line.id.toString(),
                    label: line.name,
                }
            }))
        }
    }, [productList])

    const handleChange = (value: string) => {
        console.log(`selected ${value}`);
    };

    const handleDeleteImage = async (idImg: string) => {
        await deleteImage(idImg)
        getVariantDetail(variantId)
    }

    return (
        <Form {...layout} form={form} name="control-hooks" onFinish={onFinish}>
            {variantId ? (
                <Form.Item name="name" label="Name">
                    <span>{variantDetail?.name}</span>
                </Form.Item>
            ):(<></>)}
            <Form.Item name="productId" label="Product" rules={[{ required: true }]}>

                <Select
                    style={{ width: 120 }}
                    onChange={handleChange}
                    options={optionProduct}
                />
            </Form.Item>
            <Form.Item name="color" label="Color" rules={[{ required: true }]}>
                <Input />
            </Form.Item>
            <Form.Item name="size" label="Size" rules={[{ required: true }]}>
                <Input />
            </Form.Item>
            <Form.Item label="Images">
                {variantDetail && variantDetail?.images.length > 0 && (
                    // eslint-disable-next-line react/jsx-no-undef
                    <Image.PreviewGroup>
                        {variantDetail.images.map((img, index) => {
                            // eslint-disable-next-line jsx-a11y/alt-text
                            return (
                                <Space key={index} direction="vertical">
                                    <Image
                                        width={200}
                                        src={img.url}
                                    />
                                    <Tooltip title="delete">
                                        <Button onClick={() => handleDeleteImage(img.id)} type="primary" shape="circle" icon={<DeleteOutlined />} />
                                    </Tooltip>
                                </Space>
                            )
                        })}
                    </Image.PreviewGroup>
                )}
                <UploadImageList/>
            </Form.Item>
            <Form.Item {...tailLayout}>
                <Button type="primary" htmlType="submit">
                {variantId ? "Update" : "Submit"}
                </Button>
                <Button danger onClick={onDelete}>
                Delete
                </Button>
            </Form.Item>
        </Form>
    );
};

export default VariantForm;