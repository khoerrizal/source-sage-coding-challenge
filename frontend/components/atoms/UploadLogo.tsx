import React, { useState } from "react";
import { LoadingOutlined, PlusOutlined } from "@ant-design/icons";
import { message, Upload } from "antd";
import type { UploadChangeParam } from "antd/es/upload";
import type { RcFile, UploadFile, UploadProps } from "antd/es/upload/interface";
import { convertBase64 } from "../../utils/convertBase64";
import { useGlobalContext } from "../../providers/globalProvider";

const getBase64 = (img: RcFile, callback: (url: string) => void) => {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result as string));
    reader.readAsDataURL(img);
};

const beforeUpload = (file: RcFile) => {
    const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
    if (!isJpgOrPng) {
        message.error("You can only upload JPG/PNG file!");
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
        message.error("Image must smaller than 2MB!");
    }
    return isJpgOrPng && isLt2M;
};

const UploadLogo: React.FC = () => {
    const {
        contentState: {
            setImageLogo,
            newImageUrl, 
            setNewImageUrl
        }
    } = useGlobalContext()
    const [loading, setLoading] = useState(false);

    const handleImageLogo = async (event: any) => {
        const dataB64 = await convertBase64(event.file.originFileObj) as string
        console.log(dataB64, "hasil convert<<<<<<<<<<")
        setImageLogo(dataB64.split(",")[1])
        setNewImageUrl(dataB64)
        return
    }

    const uploadButton = (
        <div>
            {loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div style={{ marginTop: 8 }}>Upload</div>
        </div>
    );

    return (
        <Upload
            name="logo"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            beforeUpload={beforeUpload}
            onChange={handleImageLogo}
        >
            {newImageUrl ? (
                <img src={newImageUrl} alt="avatar" style={{ width: "100%" }} />
            ) : (
                uploadButton
            )}
        </Upload>
    );
};

export default UploadLogo;
