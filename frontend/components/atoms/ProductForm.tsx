import React, { useEffect, useState } from 'react';
import { Button, Form, Input, Modal, Select, Space, Tooltip } from 'antd';
import { useGlobalContext } from '../../providers/globalProvider';
import { useRouter } from 'next/router';
import TextArea from 'antd/lib/input/TextArea';
import UploadLogo from './UploadLogo';
import { Image } from 'antd';
import UploadImageList from './UploadImageList';
import VariantList from '../molecules/VariantList';
import VariantForm from './VariantForm';
import { convertBase64 } from '../../utils/convertBase64';
import { DeleteOutlined } from '@ant-design/icons';


const { Option } = Select;

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },   
};
const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
};

const ProductForm: React.FC = () => {
    const {
        contentState: {
            setSelectedMenu,
            selectedMenu,
            productDetail,
            getProductDetail,
            createProduct,
            getProductList,
            updateProduct,
            setProductDetail,
            deleteProduct,
            createVariant,
            getVariantList,
            imageLogo,
            newImageUrl,
            fileList,
            setFileList,
            imagesString,
            createImage,
            deleteImage,
            setImagesString
        }
    } = useGlobalContext()
    const [form] = Form.useForm();

    const onFinish = async (values: any) => {
        if (productId){
            await updateProduct(
                productId,
                values.name,
                values.description,
                imageLogo
            )
            imagesString.map(async (img) => {
                await createImage(img, productId)
            })
            setImagesString([])
            setFileList([])
            getProductList()
            await setSelectedMenu("product-view")
            router.push("/")
            return
        }else{
            console.log(fileList, "<<<<< ce sebelum convert")
            console.log(imagesString, "<<<< cek hasil convert")
            await createProduct(
                values.name,
                values.description,
                imageLogo,
                imagesString
            )
            getProductList()
            await setSelectedMenu("product-view")
            router.push("/")
            return
        }
    };

    const onDelete = async () => {
        deleteProduct(productId)
        getProductList()
        await setSelectedMenu("product-view")
        router.push("/")
    };

    const [productId, setProductId] = useState<string | string[]>("")

    const router = useRouter()

    useEffect(() => {
        if (router.isReady && router.query.id){
            setProductId(router.query.id)
            getProductDetail(router.query.id)
        }else{
            setProductDetail(null)
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [router.isReady, router.query.id])

    useEffect(() => {
        if (selectedMenu != "product-create"){
            if (productId){
                setSelectedMenu(`product-view-${productId}`)
            }else{
                setSelectedMenu(`product-view`)
            }
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [productId, selectedMenu])

    useEffect(() => {
        console.log(productDetail, "<<< cek product detail")
        form.setFieldsValue({ 
            name: productDetail?.name,
            description: productDetail?.description
        });
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [form, productDetail])

    const [isModalOpen, setIsModalOpen] = useState(false);

    const showModal = () => {
        setIsModalOpen(true);
    };

    const handleOk = async () => {
        await createVariant(productId, form.getFieldValue('color'), form.getFieldValue('size'))
        await getVariantList()
        form.setFieldsValue({color: "", size: ""})
        setIsModalOpen(false);
    };

    const handleCancel = () => {
        setIsModalOpen(false);
    };

    const handleDeleteImage = async (idImg: string) => {
        await deleteImage(idImg)
        getProductDetail(productId)
    }

    return (
        <Form {...layout} form={form} name="control-hooks" onFinish={onFinish}>
            <Form.Item name="name" label="Name" rules={[{ required: true }]}>
                <Input/>
            </Form.Item>
            <Form.Item name="description" label="Description" rules={[{ required: true }]}>
                <TextArea rows={4} placeholder="Product description">
                </TextArea>
            </Form.Item>
            <Form.Item label="Logo">
                {productDetail?.logo && !newImageUrl && (
                    // eslint-disable-next-line jsx-a11y/alt-text
                    <Image
                        width={200}
                        src={productDetail.logo.replace("http://localhost:5000", "/")}
                    />
                )}
                <UploadLogo/>
            </Form.Item>
            <Form.Item label="Images">
                {productDetail && productDetail?.images.length > 0 && (
                    <Image.PreviewGroup>
                        {productDetail.images.map((img, index) => {
                            // eslint-disable-next-line jsx-a11y/alt-text
                            return (
                                <Space key={index} direction="vertical">
                                    <Image
                                        width={200}
                                        src={img.url}
                                    />
                                    <Tooltip title="delete">
                                        <Button onClick={() => handleDeleteImage(img.id)} type="primary" shape="circle" icon={<DeleteOutlined />} />
                                    </Tooltip>
                                </Space>
                            )
                        })}
                    </Image.PreviewGroup>
                )}
                <UploadImageList/>
            </Form.Item>
            {productId && (
                <div>
                    <Form.Item>
                        <Button type="primary" onClick={showModal}>
                            Add Variant
                        </Button>
                        <Modal title="Add Variant" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                            <Form form={form} name="control-hooks">
                                <Form.Item name="color" label="Color">
                                    <input/>
                                </Form.Item>
                                <Form.Item name="size" label="Size">
                                    <input/>
                                </Form.Item>
                            </Form>
                        </Modal>
                    </Form.Item>
                    <VariantList productId={productId.toString()}/>
                </div>
            )}
            <Form.Item {...tailLayout}>
                <Button type="primary" htmlType="submit">
                {productId ? "Update" : "Submit"}
                </Button>
                <Button danger onClick={onDelete}>
                Delete
                </Button>
            </Form.Item>
        </Form>
    );
};

export default ProductForm;