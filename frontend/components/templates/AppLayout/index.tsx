import React, { useState } from 'react';
import {
  InboxOutlined,
  CodeSandboxOutlined,
} from '@ant-design/icons';
import type { MenuProps } from 'antd';
import { Breadcrumb, Layout, Menu, theme } from 'antd';
import { useGlobalContext } from '../../../providers/globalProvider';
import ProductList from '../../molecules/ProductList';
import Head from 'next/head';
import Router from 'next/router';

const { Header, Content, Footer, Sider } = Layout;

type MenuItem = Required<MenuProps>['items'][number];

function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[],
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
  } as MenuItem;
}

const items: MenuItem[] = [
  getItem('Product', 'productMenu', <InboxOutlined />, [
    getItem('Create', 'product-create'),
    getItem('View', 'product-view'),
  ]),
  getItem('Variant', 'variantMenu', <CodeSandboxOutlined />, [
    getItem('Create', 'variant-create'),
    getItem('View', 'variant-view'),
  ]),
];


export interface AppLayoutProps {
    title: string
    description: string
    children: React.ReactNode
}

const AppLayout: React.FC<AppLayoutProps> = ({ title, description, children }) => {
    const {
        contentState: {
            selectedMenu, 
            setSelectedMenu
        }
    } = useGlobalContext()
    const [collapsed, setCollapsed] = useState(false);
    
    const {
        token: { colorBgContainer },
    } = theme.useToken();

    const handleClickMenu = (event: any) => {
        setSelectedMenu(event.key)
        Router.push("/")
    }
    return (
        <>
            <Head>
                <title>{title}</title>
                <meta name="description" content={description} />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <main>
                <Layout style={{ minHeight: '100vh' }}>
                    <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
                        <div style={{ height: 32, margin: 16, background: 'rgba(255, 255, 255, 0.2)' }} />
                        <Menu onClick={handleClickMenu} theme="dark" defaultSelectedKeys={['1']} mode="inline" items={items} />
                    </Sider>
                    <Layout className="site-layout">
                        <Header style={{ padding: 0, background: colorBgContainer }} />
                        <Content style={{ margin: '0 16px' }}>
                            <Breadcrumb style={{ margin: '16px 0' }}>
                                {selectedMenu.split("-").map((submenustring, index) => (
                                    <Breadcrumb.Item key={index} >{submenustring.toLocaleUpperCase()}</Breadcrumb.Item>
                                ))}
                            </Breadcrumb>
                            <div style={{ padding: 24, minHeight: 360, background: colorBgContainer }}>
                                {children}
                            </div>
                        </Content>
                        <Footer style={{ textAlign: 'center' }}>copyright KHOERURRIZAL@2022</Footer>
                    </Layout>
                </Layout>
            </main>
        </>
    );
};

export default AppLayout;