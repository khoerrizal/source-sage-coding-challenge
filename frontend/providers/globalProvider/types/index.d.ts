export interface IProviderProps {
    children: JSX.Element
}

export interface INotificationState {
    successMsg: string
    errorMsg: string
    setErrorMsg: (msg: string) => void
    setSuccessMsg: (msg: string) => void
}

interface dataInput {
    name: string
    value: string
}

export interface IImg {
    url: string
    id: string
}
export interface IDataProduct {
    id: string,
    name: string,
    description: string,
    logo: string | null,
    created_at: string,
    updated_at: string,
    images: IImg[]
}

export interface IDataVariant {
    id: number
    name: string,
    product_id: number,
    size: string,
    color: string,
    created_at: string,
    updated_at: string,
    images: IImg[]
}

export interface IContentState {
    activeTab: "productTab" | "variantTab"
    setActiveTab: (tab: "productTab" | "variantTab") => void
    productList: IDataProduct[]
    variantList: IDataVariant[]
    selectedMenu: string
    setSelectedMenu: (selectedMenu: string) => void
    getProductList: async
    deleteImage: async
    createImage: async
    updateVariant: async
    deleteVariant: async
    getVariantDetail: async
    createVariant: async
    getVariantList: async
    deleteProduct: async
    getProductDetail: async
    updateProduct: async
    createProduct: async
    productDetail: IDataProduct | null
    setProductDetail: (data: IDataProduct | null) => void
    variantDetail: IDataVariant | null
    setVariantDetail: (data: IDataVariant | null) => void
    imageLogo: string
    setImageLogo: (data: string) => void
    newImageUrl: string | undefined
    setNewImageUrl: (newImage: string) => void
    fileList: UploadFile[]
    setFileList: (data: UploadFile[]) => void
    imagesString: string[]
    setImagesString: (images: string[]) => void
}

export interface IGlobalState {
    notificationState: INotificationState
    contentState: IContentState
}