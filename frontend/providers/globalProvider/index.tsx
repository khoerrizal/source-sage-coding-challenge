import { UploadFile } from 'antd/es/upload/interface'
import { createContext, useContext, useEffect, useState } from 'react'
import ApiRequest from '../../config/config'
import { IApiRequest } from '../../config/types'
import { IContentState, IDataProduct, IDataVariant, IGlobalState, IImg, INotificationState, IProviderProps } from './types'

export function createCtx<IGlobalState>() {
    const ctx = createContext<IGlobalState | undefined>(undefined)
    function useCtx() {
        const c = useContext(ctx)
        if (!c) throw new Error('useCtx must be inside a Provider with a value')
        return c
    }
    return [useCtx, ctx.Provider] as const
}

export const [useGlobalContext, CtxProvider] = createCtx<IGlobalState>()

export const GlobalProvider: React.FC<IProviderProps> = ({ children }) => {
    const [successMsg, setSuccessMsg] = useState<string>('tes success')
    const [errorMsg, setErrorMsg] = useState<string>('')
    const [activeTab, setActiveTab] = useState<"productTab" | "variantTab">("productTab")
    const [productList, setProductList] = useState<IDataProduct[]>([])
    const [variantList, setVariantList] = useState<IDataVariant[]>([])
    const [selectedMenu, setSelectedMenu] = useState<string>("product-view")
    const [productDetail, setProductDetail] = useState<IDataProduct | null>(null)
    const [variantDetail, setVariantDetail] = useState<IDataVariant | null>(null)
    const [uploadedLogoId, setUploadedLogoId] = useState<number | null>(null)
    const [imageLogo, setImageLogo] = useState<string>("")
    const [newImageUrl, setNewImageUrl] = useState<string>();
    const [fileList, setFileList] = useState<UploadFile[]>([]);
    const [imagesString, setImagesString] = useState<string[]>([])

    const notificationState: INotificationState = {
        successMsg,
        errorMsg,
        setErrorMsg,
        setSuccessMsg
    }

    const getProductList = async () => {
        try {
            const payload: IApiRequest = {
                url: '/api/products/'
            }
            const { data: response} = await ApiRequest.get(payload)
            console.log(response, "response api products")
            setProductList(response.data)
            return response
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    const createProduct = async (name: string, description: string, logo?: string, images?: string[]) => {
        try {
            const payload: IApiRequest = {
                url: '/api/products/',
                body: {
                    name: name,
                    description: description,
                    logo: logo,
                    images: images
                }
            }
            const {data: response} = await ApiRequest.post(payload)
            console.log(response, "response api products")
            return response
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    const updateProduct = async (id: string, name: string, description: string, logo?: string) => {
        try {
            const payload: IApiRequest = {
                url: `/api/products/${id}`,
                body: {
                    name: name,
                    description: description,
                    logo: logo
                }
            }
            const response = await ApiRequest.put(payload)
            console.log(response, "response api products")
            return response
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    const getProductDetail = async (id: string) => {
        try {
            const payload: IApiRequest = {
                url: `/api/products/${id}`
            }
            const {data: response} = await ApiRequest.get(payload)
            if (response.images){
                response.images = response.images.map((line: IImg) => {
                    return {
                        url: line.url.replace("http://localhost:5000", "/"),
                        id: line.id
                    }
                })
            }
            if (response.logo){
                response.logo = response.logo.replace("http://localhost:5000", "/")
            }
            console.log(response, "response api detail products")
            setProductDetail(response)
            return response
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    const deleteProduct = async (id: string) => {
        try {
            const payload: IApiRequest = {
                url: `/api/products/${id}`
            }
            const response = await ApiRequest.delete(payload)
            console.log(response, "response api delete products")
            return response
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    const getVariantList = async () => {
        try {
            const payload: IApiRequest = {
                url: '/api/variants/'
            }
            const {data: response} = await ApiRequest.get(payload)
            console.log(response, "response api products")
            setVariantList(response.data)
            return response
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    const createVariant = async (productId: string, color: string, size: string, images?: string[]) => {
        try {
            const payload: IApiRequest = {
                url: '/api/variants/',
                body: {
                    product_id: productId,
                    color: color,
                    size: size,
                    images: images
                }
            }
            const response = await ApiRequest.post(payload)
            console.log(response, "response api create variant")
            return response
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    const getVariantDetail = async (id: string) => {
        try {
            const payload: IApiRequest = {
                url: `/api/variants/${id}`
            }
            const {data: response} = await ApiRequest.get(payload)
            if (response.images){
                response.images = response.images.map((line: IImg) => {
                    return {
                        url: line.url.replace("http://localhost:5000", "/"),
                        id: line.id
                    }
                })
            }
            console.log(response, "response api detail variants")
            setVariantDetail(response)
            return response
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    const deleteVariant = async (id: string) => {
        try {
            const payload: IApiRequest = {
                url: `/api/variants/${id}`
            }
            const response = await ApiRequest.delete(payload)
            console.log(response, "response api detail variants")
            return response
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    const updateVariant = async (id: string, productId: string, color: string, size: string) => {
        try {
            const payload: IApiRequest = {
                url: `/api/variants/${id}`,
                body: {
                    product_id: productId,
                    color: color,
                    size: size
                }
            }
            const response = await ApiRequest.put(payload)
            console.log(response, "response api update variant")
            return response
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    const createImage = async (image: string, product_id?: string, variant_id?: string) => {
        try {
            const payload: IApiRequest = {
                url: '/api/images/',
                body: {
                    data: image,
                    product_id: product_id,
                    variant_id: variant_id
                }
            }
            console.log(payload, "payload create image")
            const {data: response} = await ApiRequest.post(payload)
            console.log(response, "response api create images")
            setUploadedLogoId(response.data.id)
            return response
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    const deleteImage = async (id: string) => {
        try {
            const payload: IApiRequest = {
                url: `/api/images/${id}`,
            }
            const response = await ApiRequest.delete(payload)
            console.log(response, "response api delete image")
            return response
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    const contentState: IContentState = {
        activeTab, 
        setActiveTab,
        productList,
        variantList,
        selectedMenu, 
        setSelectedMenu,
        getProductList,
        deleteImage,
        createImage,
        updateVariant,
        deleteVariant,
        getVariantDetail,
        createVariant,
        getVariantList,
        deleteProduct,
        getProductDetail,
        updateProduct,
        createProduct,
        productDetail, 
        setProductDetail,
        variantDetail,
        setVariantDetail,
        imageLogo,
        setImageLogo,
        newImageUrl, 
        setNewImageUrl,
        fileList, 
        setFileList,
        imagesString, 
        setImagesString
    }

    const state: IGlobalState = {
        notificationState,
        contentState
    }

    useEffect(() => {
        getProductList()
        getVariantList()
    }, [])

    return <CtxProvider value={state}>{children}</CtxProvider>
}
