import { defaultImage } from "../../../public/form";

export const DataDetailVariant = {
    id: 1,
    name: "sepatu (Kecil, Merah)",
    product_id: 1,
    color: "Merah",
    size: "Kecil",
    created_at: "2022-12-24 02:52:36",
    updated_at: "2022-12-24 02:52:36",
    images: [
        defaultImage,
        defaultImage,
        defaultImage,
        defaultImage,
        defaultImage,
    ]
}