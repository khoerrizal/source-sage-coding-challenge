# Source Sage Coding Challenge

## Getting started

This is solution alternative of coding challenge from Source Sage.

## Description

This project provide web service to manage product and variants. You can do CRUD activity for product and variant.

## Installation

Set host ip address in docker-compose.yml in section MAIN_BASE_URL

```
- MAIN_BASE_URL=http://192.168.1.5:8080
```

Run docker compose to deploy database, backend, and frontend at once.

```
docker-compose up -d
```

## Authors and acknowledgment

-   Khoerurrizal
